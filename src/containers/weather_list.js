import React, {Component} from 'react'
import {connect} from 'react-redux'
import shortid from 'shortid'
import Chart from '../components/chart'
import Map from '../components/googleMap'

class WeatherList extends Component {

  renderWeater(cityData) {
    const temps = cityData.list.map(weather => weather.main.temp)
    const pressures = cityData.list.map(weather => weather.main.pressure)
    const humidities = cityData.list.map(weather => weather.main.humidity)
    const {lon, lat} = cityData.city.coord


    return (
      <tr key={shortid.generate()}>
        <td><Map lon={lon} lat={lat}/></td>
        <td>
          <Chart data={temps} color='red' units='°C'/>
        </td>
        <td>
          <Chart data={pressures} color="orange" units='hPa'/>
        </td>
        <td>
          <Chart data={humidities} color="blue" units='%'/>
        </td>
      </tr>
    )
  }

  render() {
    return (
      <table className='table table-hover'>
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (°C)</th>
            <th>Pressure (hPa)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>
        <tbody>
          {this.props.weather.map(this.renderWeater)}
        </tbody>
      </table>
    )
  }
}

function mapStateToProps({weather}) {
  return { weather } // === {weather: state.weather}
}

export default connect(mapStateToProps)(WeatherList)
