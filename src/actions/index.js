import axios from 'axios'

const API_KEY = '48ea9f576ba9362f154636e6d56d6246'
const BaseURL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = 'FETCH_WEATHER'

export function fetchWeather(city) {
  const url = `${BaseURL}&q=${city},us&units=metric`
  const request = axios.get(url)

  return {
    type: FETCH_WEATHER,
    payload: request
  }
}
